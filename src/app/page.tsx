'use client';
import Image from 'next/image'
import { useSelector, useDispatch } from 'react-redux';
import { increment, decrement, incrementByAmount } from '@/redux/features/counter/counterSlice';
import { RootState } from '@/redux/store';
import { Button } from '@material-tailwind/react'

export default function Home() {
  const count = useSelector((state:RootState) => state.counter.value);
  const dispatch = useDispatch();
  return (
    <main>
      <Button onClick={() => dispatch(increment())}>Increment</Button>
      <h1>{count}</h1>
      <Button onClick={() => dispatch(decrement())}>Decrement</Button>
      <Button onClick={() => dispatch(incrementByAmount(2))}>Increment by amount</Button>
    </main>
  )
}
